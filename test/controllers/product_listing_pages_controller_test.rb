require "test_helper"

class ProductListingPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_listing_page = product_listing_pages(:one)
  end

  test "should get index" do
    get product_listing_pages_url
    assert_response :success
  end

  test "should get new" do
    get new_product_listing_page_url
    assert_response :success
  end

  test "should create product_listing_page" do
    assert_difference("ProductListingPage.count") do
      post product_listing_pages_url, params: { product_listing_page: { category: @product_listing_page.category, description: @product_listing_page.description, image: @product_listing_page.image, price: @product_listing_page.price, title: @product_listing_page.title } }
    end

    assert_redirected_to product_listing_page_url(ProductListingPage.last)
  end

  test "should show product_listing_page" do
    get product_listing_page_url(@product_listing_page)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_listing_page_url(@product_listing_page)
    assert_response :success
  end

  test "should update product_listing_page" do
    patch product_listing_page_url(@product_listing_page), params: { product_listing_page: { category: @product_listing_page.category, description: @product_listing_page.description, image: @product_listing_page.image, price: @product_listing_page.price, title: @product_listing_page.title } }
    assert_redirected_to product_listing_page_url(@product_listing_page)
  end

  test "should destroy product_listing_page" do
    assert_difference("ProductListingPage.count", -1) do
      delete product_listing_page_url(@product_listing_page)
    end

    assert_redirected_to product_listing_pages_url
  end
end
