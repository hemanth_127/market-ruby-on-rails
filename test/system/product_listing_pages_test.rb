require "application_system_test_case"

class ProductListingPagesTest < ApplicationSystemTestCase
  setup do
    @product_listing_page = product_listing_pages(:one)
  end

  test "visiting the index" do
    visit product_listing_pages_url
    assert_selector "h1", text: "Product listing pages"
  end

  test "should create product listing page" do
    visit product_listing_pages_url
    click_on "New product listing page"

    fill_in "Category", with: @product_listing_page.category
    fill_in "Description", with: @product_listing_page.description
    fill_in "Image", with: @product_listing_page.image
    fill_in "Price", with: @product_listing_page.price
    fill_in "Title", with: @product_listing_page.title
    click_on "Create Product listing page"

    assert_text "Product listing page was successfully created"
    click_on "Back"
  end

  test "should update Product listing page" do
    visit product_listing_page_url(@product_listing_page)
    click_on "Edit this product listing page", match: :first

    fill_in "Category", with: @product_listing_page.category
    fill_in "Description", with: @product_listing_page.description
    fill_in "Image", with: @product_listing_page.image
    fill_in "Price", with: @product_listing_page.price
    fill_in "Title", with: @product_listing_page.title
    click_on "Update Product listing page"

    assert_text "Product listing page was successfully updated"
    click_on "Back"
  end

  test "should destroy Product listing page" do
    visit product_listing_page_url(@product_listing_page)
    click_on "Destroy this product listing page", match: :first

    assert_text "Product listing page was successfully destroyed"
  end
end
