class ProductListingPage < ApplicationRecord
  has_one_attached :image
  
  has_many :cart_items, dependent: :destroy
  has_many :users, through: :cart_items, dependent: :destroy
  validates :quantity, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
