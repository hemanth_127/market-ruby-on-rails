class User < ApplicationRecord
    has_secure_password

    validates :name, presence: true
    validates :email, presence: true, uniqueness: true

    has_many :cart_items ,dependent: :destroy 
    has_many :product_listing_pages, through: :cart_items,dependent: :destroy
end
        