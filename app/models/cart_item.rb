class CartItem < ApplicationRecord
  belongs_to :user
  belongs_to :product_listing_page
  validates :quantity, presence: true, numericality: { greater_than: 0 }
  
  scope :purchased, -> { where(order_status: "completed").order(updated_at: :desc) }
end

