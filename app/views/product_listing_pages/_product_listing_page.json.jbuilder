json.extract! product_listing_page, :id, :title, :price, :description, :category, :image, :created_at, :updated_at
json.url product_listing_page_url(product_listing_page, format: :json)
