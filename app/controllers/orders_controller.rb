class OrdersController < ApplicationController
  def index
    if current_user.present?
      @orders = current_user.cart_items.purchased
    end
  end
end
