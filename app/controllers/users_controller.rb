class UsersController < ApplicationController
  before_action :authenticate_admin!, only: %i[index edit update destroy]

  def index
    if current_user.present?
      @cart_items = current_user.cart_items.includes(:product_listing_page).where(order_status: "pending")
    end
    @users = User.all
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)

    puts "akgami #{current_user&.admin}"
    if current_user&.admin?
      if @user.save
        redirect_to users_path, notice: "User #{@user.name} created successfully."
      else
        render :new
      end
    elsif @user.save
      session[:user_id] = @user.id
      redirect_to root_path
    else
      render :new
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      redirect_to users_path, notice: "User updated successfully."
    else
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy!
    redirect_to users_path, notice: "User deleted successfully."
  end

  def purchased_products
    @user = User.find(params[:id])
    @purchased_products = @user.cart_items.purchased
    puts "god mode $ #{@purchased_products}"
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :phone_number, :address)
  end
end
