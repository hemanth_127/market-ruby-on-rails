class CartItemsController < ApplicationController
  before_action :require_login

  def index
    @cart_items = current_user.cart_items.includes(:product_listing_page).where(order_status: "pending")
    @total_price = @cart_items.sum { |cart_item| cart_item.product_listing_page.price }
  end

  def create
    product = ProductListingPage.find(params[:product_listing_page_id])
    cart_item = current_user.cart_items.find_or_initialize_by(product_listing_page: product, order_status: "pending")
    if cart_item.new_record?
      cart_item.save
      redirect_to product_listing_pages_path, notice: "Product added to cart."
    elsif product.quantity > cart_item.quantity
      cart_item.quantity += 1
      cart_item.save
      redirect_to product_listing_pages_path, notice: "Product quantity updated in cart."
    else
      redirect_to product_listing_pages_path, alert: "Product out of stock."
    end
  end

  def increment
    current_cart_item = current_user.cart_items.find(params[:id])
    product = current_cart_item.product_listing_page

    if product.quantity > current_cart_item.quantity
      current_cart_item.increment!(:quantity)
      flash[:notice] = "Product quantity updated."
    else
      flash[:alert] = "Product quantity exceeds available stock."
    end

    redirect_to cart_items_path
  end

  def decrement
    cart_item = current_user.cart_items.find(params[:id])
    if cart_item.quantity > 1
      cart_item.decrement!(:quantity)
      redirect_to cart_items_path, notice: "Product decremented updated."
    else
      redirect_to cart_items_path, notice: "you should click remove "
    end
  end

  def destroy
    cart_item = current_user.cart_items.find(params[:id])
    cart_item.destroy
    redirect_to cart_items_path, notice: "Product removed from cart."
  end

  # def purchase
  #   @cart_items = current_user.cart_items.includes(:product_listing_page).where(order_status: "pending")

  #   @cart_items.each do |cart_item|
  #     product = cart_item.product_listing_page

  #     if product.quantity >= cart_item.quantity
  #       product.update(quantity: product.quantity - cart_item.quantity)
  #       cart_item.update(order_status: "completed", updated_at: Time.current) # Use updated_at as purchase date
  #     else
  #       flash[:alert] = "Not enough stock for #{product.title}. Purchase aborted."
  #       redirect_to cart_items_path and return
  #     end
  #   end

  #   flash[:notice] = "Purchase completed successfully."
  #   redirect_to cart_items_path
  # end

  def purchase
    @cart_items = current_user.cart_items.includes(:product_listing_page).where(order_status: "pending")

    if request.post?
      @cart_items.each do |cart_item|
        product = cart_item.product_listing_page

        if product.quantity >= cart_item.quantity
          product.update(quantity: product.quantity - cart_item.quantity)
          cart_item.update(order_status: "completed", updated_at: Time.current) # Use updated_at as purchase date
        else
          flash[:alert] = "Not enough stock for #{product.title}. Purchase aborted."
          redirect_to cart_items_path and return
        end
      end

      current_user.update(address: params[:address], phone_number: params[:phone_number])

      flash[:notice] = "Purchase completed successfully."
      redirect_to cart_items_path
    else
      # Render the form for address and phone number
    end
  end

  private

  def require_login
    unless logged_in?
      flash[:error] = "You must be logged in to access this section"
      redirect_to login_path
    end
  end

  def cart_item_params
    params.require(:cart_item).permit(:quantity)
  end
end
