class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)

    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      redirect_to root_path, notice: "Logged in successfully."
    else
      Rails.logger.info "[SessionsController::create] else user = #{user.inspect}"
      flash[:alert] = "Invalid email/password combination"
      Rails.logger.info "[SessionsController::create] else flash = #{flash.inspect}"
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path, notice: "Logged out successfully."
  end
end
