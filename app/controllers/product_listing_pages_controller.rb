class ProductListingPagesController < ApplicationController
  before_action :set_product_listing_page, only: %i[ destroy show edit update ]
  before_action :authenticate_admin!, only: %i[new create edit update destroy]

  # GET /product_listing_pages
  def index
    @product_listing_pages = ProductListingPage.all

    if current_user.present?
      @cart_items = current_user.cart_items.includes(:product_listing_page).where(order_status: "pending")
    end

    if params[:category].present? && params[:category] != "All"
      @product_listing_pages = @product_listing_pages.where("LOWER(category) = ?", params[:category].downcase)
    end

    if params[:pricerange].present?
      price_range = params[:pricerange].split("-")
      Rails.logger.info "[productController::index] = #{price_range}"
      @product_listing_pages = @product_listing_pages.where("price BETWEEN ? AND ?", price_range[0].to_i, price_range[1].to_i)
    end
  end


  # GET /product_listing_pages/1
  def show
    if current_user.present?
      @cart_items = current_user.cart_items.includes(:product_listing_page).where(order_status: "pending")
    end
    @override_image = true
  end

  # GET /product_listing_pages/neproduct_listing_pagew
  def new
    if current_user.present?
      @cart_items = current_user.cart_items.includes(:product_listing_page).where(order_status: "pending")
    end

    @product_listing_page = ProductListingPage.new
  end

  # GET /product_listing_pages/1/edit
  def edit
  end

  # POST /product_listing_pages
  def create
    @product_listing_page = ProductListingPage.new(product_listing_page_params)

    respond_to do |format|
      if @product_listing_page.save
        format.html { redirect_to product_listing_page_url(@product_listing_page), notice: "Product listing page was successfully created." }
        format.json { render :show, status: :created, location: @product_listing_page }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @product_listing_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_listing_pages/1
  def update
    respond_to do |format|
      if @product_listing_page.update(product_listing_page_params)
        format.html { redirect_to product_listing_page_url(@product_listing_page), notice: "Product listing page was successfully updated." }
        format.json { render :show, status: :ok, location: @product_listing_page }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @product_listing_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_listing_pages/1
  def destroy
    @product_listing_page.destroy!

    respond_to do |format|
      format.html { redirect_to product_listing_pages_url, notice: "Product listing page was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use common setup or constraints between actions.
  def set_product_listing_page
    @product_listing_page = ProductListingPage.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def product_listing_page_params
    params.require(:product_listing_page).permit(:title, :price, :description, :category, :image, :quantity)
  end

  # def authenticate_admin!
  #   unless current_user&.admin?
  #     redirect_to root_path, alert: "You are not authorized to perform this action."
  #   end
  # end
end
