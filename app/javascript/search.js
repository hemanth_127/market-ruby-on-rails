document.addEventListener('DOMContentLoaded', function () {
    const input = document.querySelector("#search-product");
    const products = document.querySelectorAll('.product-details');
    let timeout;

    input.addEventListener("input", debounce(handleInput, 500));

    function handleInput(e) {
        const searchQuery = e.target.value.trim().toLowerCase();
        filterProducts(searchQuery);
    }

    function filterProducts(query) {
        products.forEach(function (page) {
            const titleElement = page.querySelector('.detail h2');
            if (titleElement) {
                const title = titleElement.textContent.toLowerCase();
                const show = title.includes(query);
                page.style.display = show ? 'block' : 'none';
            }
        });
    }

    function debounce(cb, delay = 500) {
        return function (...args) {
            clearTimeout(timeout);
            timeout = setTimeout(() => {
                cb(...args);
            }, delay);
        };
    }
});
