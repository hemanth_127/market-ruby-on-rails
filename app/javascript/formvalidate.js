document.getElementById('productForm').onsubmit = function (event) {
    event.preventDefault();
    const inputValues = {};
    const inputs = document.querySelectorAll('.form-field');

    inputs.forEach(input => {
        inputValues[input.name] = input.value.trim();
    });

    let isValid = true;
    const errorMessages = {};
    const errorElements = document.querySelectorAll('.error');

    errorElements.forEach(errorElement => {
        errorElement.style.display = 'none';
    });

    for (const i in inputValues) {
        match = i.match(/\[([^\]]+)\]/)[1]
        if (!inputValues[`product_listing_page[${match}]`]) {
            errorMessages[`${match}`] = `please enter the ${match}`;
            isValid = false;
        }
    }

    Object.keys(errorMessages).forEach(fieldName => {
        const errorMessageElement = document.querySelector(`.error.${fieldName}-error`);
        errorMessageElement.textContent = errorMessages[fieldName];
        errorMessageElement.style.display = 'block';
    });

    if (isValid) {
        event.target.submit();
    }
};



