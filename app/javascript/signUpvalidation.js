document.addEventListener("DOMContentLoaded", () => {
    const form = document.querySelector("#signup-form");

    form.addEventListener("submit", function (event) {
        let valid = true;
        const name = document.querySelector("#user_name");
        const email = document.querySelector("#user_email");
        const password = document.querySelector("#user_password");
        const passwordConfirmation = document.querySelector("#user_password_confirmation");
        const phoneNumber = document.querySelector("#user_phone_number");
        const address = document.querySelector("#user_address");


        clearErrors();

        if (name.value.trim() === "") {
            valid = false;
            showError(name, "name-error");
        }

        if (email.value.trim() === "" || !validateEmail(email.value)) {
            valid = false;
            showError(email, "email-error");
        }

        if (password.value.length < 6) {
            valid = false;
            showError(password, "password-error");
        }

        if (password.value !== passwordConfirmation.value) {
            valid = false;
            showError(passwordConfirmation, "password_confirmation-error");
        }

        if (phoneNumber.value.trim().length !== 10) {
            valid = false;
            showError(phoneNumber, "phone_number-error");
        }

        if (address.value.trim() === "") {
            valid = false;
            showError(address, "address-error");
        }

        if (!valid) {
            event.preventDefault();
        }
    });

    function validateEmail(email) {
        const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return re.test(String(email).toLowerCase());
    }

    function showError(inputField, errorId) {
        inputField.classList.add("error");
        document.getElementById(errorId).style.display = "block";
    }

    function clearErrors() {
        const errorMessages = document.querySelectorAll(".error-message");
        const errorFields = document.querySelectorAll(".error");

        errorMessages.forEach((error) => {
            error.style.display = "none";
        });

        errorFields.forEach((field) => {
            field.classList.remove("error");
        });
    }
});

