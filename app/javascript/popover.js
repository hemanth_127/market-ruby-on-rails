var openModalButtons = document.querySelectorAll('[data-modal-target]')
var closeModalButtons = document.querySelectorAll('[data-close-button]')
var overlay = document.getElementById('overlay')
var submitClose = document.getElementById('close-submit')

openModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = document.querySelector(button.dataset.modalTarget)
        openModal(modal)
    })
})

if (submitClose) {
    submitClose.addEventListener('click', (event) => {
        event.preventDefault();
        const modals = document.querySelectorAll('.modal.active');
        modals.forEach(modal => {
            closeModal(modal);
        });
    });
}
overlay.addEventListener('click', () => {
    const modals = document.querySelectorAll('.modal.active')
    modals.forEach(modal => {
        closeModal(modal)
    })
})

closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modal')
        closeModal(modal)
    })
})

function openModal(modal) {
    if (modal == null) return
    modal.classList.add('active')
    overlay.classList.add('active')
}

function closeModal(modal) {
    if (modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')
}






// <%# <%= form_tag(product_listing_pages_path, method: :get) do %>
//     <div>
//       <%= text_field_tag :search, params[:search], placeholder: "Search",class:'search-input'  %>
//     </div>
//   <% end %> %>