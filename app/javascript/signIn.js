document.addEventListener("DOMContentLoaded", function () {

    var loginForm = document.getElementById("sign-form");

    loginForm.addEventListener("submit", function (event) {

        var emailInput = document.getElementById("session_email");
        var passwordInput = document.getElementById("session_password");
        var emailError = document.getElementById("email-error");
        var passwordError = document.getElementById("password-error");
        var isValid = true;

        if (!emailInput.value) {
            emailError.style.display = "block";
            isValid = false;
        } else {
            emailError.style.display = "none";
        }   

        if (!passwordInput.value) {
            passwordError.style.display = "block";
            isValid = false;
        } else {
            passwordError.style.display = "none";
        }

        if (!isValid) {
            event.preventDefault();
        }
    });
});
