class AddAddressAndPhoneNumberToUsers < ActiveRecord::Migration[7.1]
  def change
    unless column_exists?(:users, :address)
      add_column :users, :address, :string
    end

    unless column_exists?(:users, :phone_number)
      add_column :users, :phone_number, :string
    end
  end
end
