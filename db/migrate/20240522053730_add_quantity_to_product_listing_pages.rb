class AddQuantityToProductListingPages < ActiveRecord::Migration[7.1]
  def change
    add_column :product_listing_pages, :quantity, :integer
  end
end
