class CreateProductListingPages < ActiveRecord::Migration[7.1]
  def change
    create_table :product_listing_pages do |t|
      t.string :title
      t.float :price
      t.text :description
      t.string :category
      t.string :image

      t.timestamps
    end
  end
end
