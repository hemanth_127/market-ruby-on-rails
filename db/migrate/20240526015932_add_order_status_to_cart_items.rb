class AddOrderStatusToCartItems < ActiveRecord::Migration[7.1]
  def change
    add_column :cart_items, :order_status, :string, default: "pending", null: false
  end
end
