# Pin npm packages by running ./bin/importmap

pin "application"
pin "@hotwired/turbo-rails", to: "turbo.min.js"
pin "@hotwired/stimulus", to: "stimulus.min.js"
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js"
pin_all_from "app/javascript/controllers", under: "controllers"


pin "formvalidate", to: "formvalidate.js"
pin "flasher", to:"flasher.js" 

pin "popover", to:"popover.js" 
pin "search", to:"search.js"
pin "signUpvalidation" , to:"signUpvalidation.js"
pin "signIn" , to:"signIn.js"