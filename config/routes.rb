Rails.application.routes.draw do
  get "orders/index"
  resources :product_listing_pages

  root "product_listing_pages#index"

  get "signup", to: "users#new", as: "signup"
  post "signup", to: "users#create"

  get "login", to: "sessions#new", as: "login"
  post "login", to: "sessions#create"
  delete "/logout", to: "sessions#destroy"

  resources :users, only: [:index, :destroy, :update, :edit] do
    get "purchased_products", to: "users#purchased_products", on: :member
  end

  resources :orders, only: [:index]

  resources :cart_items, only: [:index, :create, :update, :destroy, :show] do
    member do
      patch :increment
      patch :decrement
    end
    collection do
      match :purchase, via: [:get, :post]
      # post :purchase
    end
  end

  get "up" => "rails/health#show", as: :rails_health_check
end
